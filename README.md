This is an interactive door game using primarily JavaScript. 

In the directory you'll find a screenshot of the project and here, too:
https://xiphosx.gitlab.io/js_door_game

About this game: Hiding behind oneo of these doors is a chorebot. Your mission is to open all of the doors without running into chorebot. If you manage to avoid the chorebot until you open the very last door, you win! 

See if you can score a winning streak!